import os
import re

import pandas as pd


class Dataprocessor:
    def __init__(self, training_data_path='./Trainning', training_plk_name='Train_all.pkl',
                 testing_data_path='./Test', testing_pkl_name='test.pkl'):
        if training_plk_name not in os.listdir('./'):
            self.loadTrainingData(training_data_path=training_data_path, training_pkl_name=training_plk_name)
        else:
            self.train_df = pd.read_pickle(training_plk_name)
        if testing_pkl_name not in os.listdir('./'):
            self.loadTestData()
        else:
            self.testing_df = pd.read_pickle(testing_pkl_name)

    def loadTrainingData(self, training_data_path='./Trainning', training_pkl_name='Train_all.pkl'):
        categories = []
        for dir in os.listdir(training_data_path):
            if not dir == 'Answer_of_Trainning':
                categories.append(re.sub('_cut', '', dir))
        train_list = []
        self.category_idx = {}
        for i in range(len(categories)):
            self.category_idx[categories[i]] = i
        for category in categories:
            idx = self.category_idx[category]
            path = training_data_path + '/' + category + '_cut'
            for filename in os.listdir(path):
                filepath = path + '/' + filename
                with open(filepath, encoding='utf-8') as file:
                    words = file.read().strip().split(' / ')
                    train_list.append([words, idx])
        self.train_df = pd.DataFrame(train_list, columns=['text', 'category'])
        self.train_df.to_pickle(training_pkl_name)

    def loadTestData(self, testing_data_path='./Test', testing_pkl_name='test.pkl'):
        testData = []
        for filename in os.listdir(testing_data_path):
            id = int(re.sub('.txt', '', filename))
            filepath = testing_data_path + '/' + filename
            with open(filepath, encoding='utf-8') as file:
                words = file.read().strip().split(' / ')
                testData.append([id, words])
        testing_df = pd.DataFrame(testData, columns=['id', 'text'])
        testing_df.to_pickle(testing_pkl_name)

    def getAlltext(self):
        corpus = pd.concat([self.train_df.text, self.testing_df.text])
        return corpus
