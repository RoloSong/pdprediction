import pandas as pd
from gensim.models import word2vec
from DataProcesser import Dataprocessor
import os

class modelBuilder:
    def __init__(self, dataManager:Dataprocessor):
        self.dataManager = dataManager
        if "allInOne.model" not in os.listdir('./'):
            self.build_all_in_one_model()
        else:
            self.model = word2vec.Word2Vec.load('allInOne.model')

    def build_all_in_one_model(self, model_name = 'allInOne.model'):
        corpus = self.dataManager.getAlltext()
        model = word2vec.Word2Vec(corpus, size=300, iter = 15, workers=7, min_count=1)
        model.save(model_name)